import { lines, labels, transfers, stations } from './map/metro.json';
import interact from 'interactjs';

const widthMap = 1040;
const heightMap = 1080;

const getPortWidth = () => document.querySelector('.map-viewport').offsetWidth;
const getPortHeght = () => document.querySelector('.map-viewport').offsetHeight;

// selected stations array
let selectedStations = [];

// disabled stations array
let disabledStations = [];

let zoomRatio = 1;

let widthPort = getPortWidth();
let heightPort = getPortHeght();

let widthZoomedMap = widthMap * zoomRatio;
let heightZoomedMap = heightMap * zoomRatio;

let widthWrapper = 2 * widthZoomedMap - widthPort;
let heightWrapper = 2 * heightZoomedMap - heightPort;

let deltaX = widthZoomedMap - widthPort;
let deltaY = heightZoomedMap - heightPort;

const calcMap = () => {
    widthPort = getPortWidth();
    heightPort = getPortHeght();

    widthZoomedMap = widthMap * zoomRatio;
    heightZoomedMap = heightMap * zoomRatio;

    widthWrapper = 2 * widthZoomedMap - widthPort;
    heightWrapper = 2 * heightZoomedMap - heightPort;

    deltaX = widthZoomedMap - widthPort;
    deltaY = heightZoomedMap - heightPort;

    document.querySelector('.map').style.width = widthZoomedMap + 'px';
    document.querySelector('.map').style.height = heightZoomedMap + 'px';

    document.querySelector('.map-wrapper').style.width = widthWrapper + 'px';
    document.querySelector('.map-wrapper').style.height = heightWrapper + 'px';
    document.querySelector('.map-wrapper').style.left = -deltaX + 'px';
    document.querySelector('.map-wrapper').style.top = -deltaY + 'px';

}

const positionMap = () => {
    let map = document.querySelector('#map');

    map.style.transform = ("translate(" + deltaX + "px," + deltaY + "px)");
    map.setAttribute('data-x', deltaX);
    map.setAttribute('data-y', deltaY);
}

const checkZoom = ratio => (widthPort < widthMap * ratio) && (heightPort < heightMap * ratio);

const handleZoomUp = () => {

    if ( checkZoom(zoomRatio + 0.1) && zoomRatio < 2 ) {
        zoomRatio = zoomRatio + 0.1;
        calcMap();

        document.querySelector('#map svg').style.width = widthZoomedMap;
        document.querySelector('#map svg').setAttribute('width', widthZoomedMap, "");
        document.querySelector('#map svg').style.height = heightZoomedMap;
        document.querySelector('#map svg').setAttribute('height', heightZoomedMap, "");
       
    }
}

const handleZoomDown = () => {    
    if ( checkZoom(zoomRatio - 0.1) ) {
        zoomRatio = zoomRatio - 0.1;
        
        calcMap();

        document.querySelector('#map svg').style.width = widthZoomedMap;
        document.querySelector('#map svg').setAttribute('width', widthZoomedMap);
        document.querySelector('#map svg').style.height = heightZoomedMap;
        document.querySelector('#map svg').setAttribute('height', heightZoomedMap);

        positionMap();

        console.log('--- zoom:', zoomRatio);

    }
}

document.querySelector('#zoom-up').addEventListener('click', handleZoomUp);
document.querySelector('#zoom-down').addEventListener('click', handleZoomDown);

calcMap();

let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
// svg.setAttribute('style', 'border: 1px solid black');
svg.setAttribute('width', widthZoomedMap);
svg.setAttribute('height', heightZoomedMap);
svg.setAttribute('viewBox', `0 0 ${widthMap} ${heightMap}`);
svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");

const getNode = (n, v) => {
    n = document.createElementNS("http://www.w3.org/2000/svg", n);
    for (var p in v)
      n.setAttributeNS(null, p.replace(/[A-Z]/g, function(m, p, o, s) { return "-" + m.toLowerCase(); }), v[p]);
    return n
}

const getStations = (arr) => {
    let id = 1;
    return arr.reduce( (acc, entity) => ({
    ...acc, ['st-' + id++]: entity
    }), {} )
}

const getSelectOptions = (obj) => {
    return Object.keys($stations).map( id => ({
        label: $stations[id].text.title || $stations[id].text.fulltitle,
        value: id
    }))
}

// handle map clicking
const handleStationClick = (id) => {
    
    const index = selectedStations.indexOf(id);
    const elem = document.getElementById(id);

    if (elem.classList.contains('disabled')) return;
    if (index === -1) {
        selectedStations.push(id);
        elem.classList.add("checked");
    } else {
        selectedStations.splice(index, 1);
        elem.classList.remove("checked");
    }

    handleSelectedList();
    handleSelect();
};

// display selected stations
const handleSelectedList = () => {
    let selectedList = document.querySelector('.selected-stations');

    selectedList.innerHTML = '';

    selectedStations.forEach( id => {
        const li = document.createElement('li');
        const title = $stations[id].text.title || $stations[id].text.fulltitle;
        li.innerHTML = `<span class="delete-station" data-id=${id}>-</span>` + title;
        
        selectedList.appendChild(li);
    })
}

// display non-selected stations into SELECT
const handleSelect = () => {
    let select = document.querySelector('#stations-select');
    select.innerHTML = '';  

    let option = document.createElement('option');
    option.value = 0;
    option.innerHTML = 'Select subway station';
    select.appendChild(option);
    
    Object.keys($stations).forEach( id => {

        if (disabledStations.indexOf(id) !== -1) return;    //  исключаем  disable stations в Select-e

        const index = selectedStations.indexOf(id);
        if (index === -1) {
            const option = document.createElement('option');
            option.value = id;
            option.innerHTML = $stations[id].text.title || $stations[id].text.fulltitle;

            select.appendChild(option);
        }
    })
}

const $stations = getStations(labels);
const $stationNames = getSelectOptions($stations);


// fill SELECT by non-selected stations
handleSelect();

// add listener to SELECT
document.querySelector('#stations-select').addEventListener('change', e => {
    const id = e.target.value;
    handleStationClick(id);
})

// add listener to UL (delete station button)
document.querySelector('.selected-stations').addEventListener('click', e => {
    const id = e.target.getAttribute('data-id');

    if (id) {
        handleStationClick(id);
    }
})

// add listener to clear Button
document.querySelector('#clear').addEventListener('click', () => {
    selectedStations = [];

    handleSelectedList();
    handleSelect();

    document.querySelectorAll('text').forEach( item => item.classList.remove('checked') );    
})

//   set METRO LINES
let g = getNode('g', {id: "scheme-layer-lines"});
lines.forEach( item => {
    const r = getNode('path', { id: item.id, d: item.d, stroke: item.stroke });
    g.appendChild(r);
})
svg.appendChild(g);


//   set METRO TRANSFERS
g = getNode('g', {id: "scheme-layer-transfers"});
transfers.forEach( item => {
    const r = getNode('path', { id: item.id, d: item.d });
    g.appendChild(r);
})
svg.appendChild(g);

// set METRO STATIONS
g = getNode('g', {id: "scheme-layer-stations"});
stations.forEach( item => {
    const r = getNode('circle', { id: item.id, cx: item.cx, cy: item.cy, r: 5, fill: item.fill });
    g.appendChild(r);
})
svg.appendChild(g);

// set METRO LABELS
g = getNode('g', {id: "scheme-layer-labels"});

Object.keys($stations).forEach( id => {
    const { rect, text } = $stations[id];
    const { tspan_1, tspan_2, tspan_3} = $stations[id];

    const complexTitle = text.title || text.fulltitle;

    const r = getNode('rect', { x: rect.x, y: rect.y, width: rect.width, height: rect.height, rx: rect.rx, ry: rect.ry });
    
    let txt = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    txt.setAttributeNS(this, "x", text.x);
    txt.setAttributeNS(this, "y", text.y);
    txt.setAttributeNS(this, "text-anchor", text.anchor);
    txt.setAttributeNS(this, "id", id);

    if (text.checked === 1) {
        txt.setAttributeNS(this, "class", 'checked');
        selectedStations.push(id);
    }

    if (text.disabled === 1) {
        txt.setAttributeNS(this, "class", 'disabled');
        disabledStations.push(id);
    }

    // txt.addEventListener("click", () => handleStationClick(id), false);
    
    // station has multi-line title
    if (!text.title) {
        // first part of multi-line station title
        const span = document.createElementNS("http://www.w3.org/2000/svg", 'tspan');
        span.setAttributeNS(null, "x", tspan_1.x);
        span.setAttributeNS(null, "dy", tspan_1.dy);
        span.setAttributeNS(null, "title", tspan_1.title);
        span.setAttributeNS(null, "data-id", id);

        const textNode = document.createTextNode(tspan_1.title);
        span.appendChild(textNode);
        txt.appendChild(span);

        // second part of multi-line station title
        const span_2 = document.createElementNS("http://www.w3.org/2000/svg", 'tspan');
        span_2.setAttributeNS(null, "x", tspan_2.x);
        span_2.setAttributeNS(null, "dy", tspan_2.dy);
        span_2.setAttributeNS(null, "title", tspan_2.title);
        span_2.setAttributeNS(null, "data-id", id);

        const textNode_2 = document.createTextNode(tspan_2.title);
        span_2.appendChild(textNode_2);
        txt.appendChild(span_2);

        // third part of multi-line station title
        if (tspan_3) {
            const span_3 = document.createElementNS("http://www.w3.org/2000/svg", 'tspan');
            span_3.setAttributeNS(null, "x", tspan_3.x);
            span_3.setAttributeNS(null, "dy", tspan_3.dy);
            span_3.setAttributeNS(null, "title", tspan_3.title);
            span_3.setAttributeNS(null, "data-id", id);
    
            const textNode_3 = document.createTextNode(tspan_3.title);
            span_3.appendChild(textNode_3);
            txt.appendChild(span_3);
        }


    } else {
        const textNode = document.createTextNode(text.title);
        txt.appendChild(textNode);
    }
    
    g.appendChild(r);
    g.appendChild(txt);

})

svg.appendChild(g);

document.querySelector("#map").appendChild(svg);

handleSelectedList();
handleSelect();

positionMap();

interact('.draggable')
.draggable({
  onmove: dragMoveListener,
});

function dragMoveListener (event) {
    var target = event.target;

    let coorX = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
    let coorY = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    if (coorX < 0) { coorX = 0 }
    if (coorY < 0) { coorY = 0 }

    if (coorX > deltaX-1 ) { coorX = deltaX-1 }
    if (coorY > deltaY-1 ) { coorY = deltaY-1 }
    
    // translate the element
    target.style.webkitTransform =
    target.style.transform =
      'translate(' + coorX + 'px, ' + coorY + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', coorX);
    target.setAttribute('data-y', coorY);
   
  }


window.addEventListener('resize', calcMap);

// open mobile map listener 

document.querySelector('#show-map-mobile').addEventListener('click', () => {
    document.querySelector('.main').classList.add('map-active');
})

document.querySelector('#hide-mobile-map').addEventListener('click', () => {
    document.querySelector('.main').classList.remove('map-active');
})

document.querySelector('.map').addEventListener('click', event => {
    const target = event.target

    if (target.nodeName === 'text' || target.nodeName === 'tspan') {
        const id = target.getAttribute('id') || target.getAttribute('data-id');

        handleStationClick(id);   
    }
}, false);

// API

export const isDisabled = id => disabledStations.indexOf(id) !== -1

export const toggleDisabled = id => {
    const index = disabledStations.indexOf(id);
    const elem = document.getElementById(id);
    
    if (index === -1) {
        disabledStations.push(id);
        elem.classList.add("disabled");
        elem.classList.remove("checked");
    } else {
        disabledStations.splice(index, 1);
        elem.classList.remove("disabled");
    }

    handleSelectedList();
    handleSelect();
}

export const isChecked = id => selectedStations.indexOf(id) !== -1

export const toggleChecked = id => {
    const index = selectedStations.indexOf(id);
    const elem = document.getElementById(id);
    
    if (index === -1) {
        selectedStations.push(id);
        elem.classList.add("checked");
        elem.classList.remove("disabled");
    } else {
        selectedStations.splice(index, 1);
        elem.classList.remove("checked");
    }

    handleSelectedList();
    handleSelect();
}
const path = require('path');
const webpack = require('webpack');

module.exports = {
    context: __dirname + '/src',
    
    entry: "./app",

    output: {
        path: path.resolve(__dirname, "public/js"),
        publicPath: "/js/",
        filename: 'app.js',       
        library: "app"
    },

    watch: true,
    watchOptions: {
        aggregateTimeout: 100,
        poll: 1000,
        ignored: /node_modules/
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015', 'stage-0'],
                        plugins: ['transform-runtime']
                    }
                }
            }
        ]
    }
}